import { Injectable } from '@angular/core';
import { Question } from './question.model';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

// import 'rxjs/add/operator/toPromise';
/****
        Use urljoin
/***/
@Injectable()
export class QuestionService {

    private questionUrl: string;

    constructor(private http: HttpClient) { 
        this.questionUrl = `${environment.apiUrl}/questions`;
     }

    getQuestions(): Promise<void | Question[]> {
        return this.http.get(this.questionUrl)
            .toPromise()
            .then(response => response as Question[])
            .catch(this.handleError)
    }

    getQuestionId(id): Promise<Question | void> {
        return this.http.get(`${this.questionUrl}/id=${id}`)
        .toPromise()
        .then(response => response as Question)
        .catch(this.handleError)
    }

    saveQuestion(question: Question) {
        const body = JSON.stringify(question);
        const headers = new HttpHeaders({'Content-Type':'application/json'});
        return this.http.post(this.questionUrl, body, { headers })
            .pipe(
                map((response) => response),
                catchError(err => {
                    console.log("ppe")
                    return err
                })
            )
            // .catch((err) => this.handleError(err));
            // .subscribe(
            //     (next) => {
            //         console.log(next);
            //     },(error) => {
            //         console.log(error);
            //     },() => {
            //         console.log("termino");
            //     }
            // )
            // .toPromise()
            // .then(res => res as Question)
            // .catch(error => this.handleError(error));
    }    

    handleError(error: any) {
        const errMsg = error.message ? error.message:
                error.status ? `${error.status} - ${error.statusText}`:
                'Server Error kr';
        console.log(errMsg)
    }

}